# Soutěž na Kaggle: Quora question pairs

https://www.kaggle.com/competitions/quora-question-pairs/overview

## Zadání
Úkolem je využít metody strojového učení na automatické rozpoznávání duplicity otázek ze serveru Quora. K dispozici je dataset složený ze dvojic otázek a informací o tom zda je jejich význam stejný, cílem je dosáhnout co největší přesnosti při určování zda je jejich význam shodný.

## Data
Všechna data jsou uložena na následujícím google disku. 
https://drive.google.com/drive/folders/1twi8D3DQ7WsgmmEllHCVgmHt0TIXFKlv?usp=share_link

## Skripty
Skripty pro práci s daty píšu zároveň na google disku do Google Colab a zároveň na lokálním počítači.
Lokální skripty jsou aktuálne:
EmbedData.py -> tím jsem vytvořil z vět vektorovou reprezentaci
SampleData.py a SampleData02.py -> pro vytvoření menšího výběru z dat pro snažší práci a rychlejší výpočty
countSimilarities.py -> pro spočítání cosinové podobnosti na mém výběru dat

Skripty z Google Colab:
https://colab.research.google.com/drive/1gVS7tvsRPvUBWl8adNTRLULhK6zsR6H1?usp=sharing -> průzkum a klasifikace základních dat
