#This script makes a sample of the training data
import pandas as pd

data = pd.read_csv('train.csv')
data = data.dropna()
sampled = data.sample(n=int(len(data) / 4))
sampled.info()
print(sampled.describe())

sampled.to_csv("sampled_train.csv")

df = pd.read_csv('EmbeddedData.csv')
df = df[df[''].isin([3, 6])]
