#This script calculates the cosine similartiy for all sentences in the sampled training set
import pandas as pd
import numpy as np
from sentence_transformers import util

data = pd.read_csv('sampled_embeddings.csv')
df = pd.read_csv('sampled_train.csv')
data = data.drop(columns=['Unnamed: 0'])
c = 0
res = pd.DataFrame(columns = ["id", "is_duplicate", "cos_sim"])
for index, row in df.iterrows():
    c += 1
    if c % 1000 == 0:
        print(c)
    cosine_score = util.cos_sim(data[data['id'] == row['qid1']].drop(columns=['id']).to_numpy()[0],
                            data[data['id'] == row['qid2']].drop(columns=['id']).to_numpy()[0])
    res.loc[len(res.index)] = [row["id"], row['is_duplicate'], cosine_score.numpy()[0][0]]

res.to_csv("sampled_cosine_similarity_scores.csv")