#This script embeds all training data with BERT

import pandas as pd
import numpy as np
from sentence_transformers import SentenceTransformer, util

data = pd.read_csv('train.csv')
# There are three rows with null values
data = data.dropna()

model = SentenceTransformer('all-MiniLM-L6-v2')


embs = pd.DataFrame(columns = ["id"] + ["emb_" + str(x) for x in range(0,384)])
usedIds = set()

c = 0
for index, row in data.iterrows():
    c += 1
    if c % 1000 == 0:
        print(c)
    if row['qid1'] not in usedIds:
        embeding = model.encode(row['question1'])
        embeding = np.insert(embeding, 0, row['qid1'], axis=0)
        embs.loc[len(embs.index)] = embeding
        usedIds.add(row['qid1'])
    if row['qid2'] not in usedIds:
        embeding = model.encode(row['question2'])
        embeding = np.insert(embeding, 0, row['qid2'], axis=0)
        embs.loc[len(embs.index)] = embeding
        usedIds.add(row['qid2'])

embs.to_csv("EmbeddedData.csv")