#This script makes a set with all the sentences in the sampled training data
import pandas as pd

data = pd.read_csv('sampled_train.csv')

id1 = data['qid1'].unique().tolist()
id2 = data['qid2'].unique().tolist()

ids = id1 + id2
print(len(ids))
ids = list(dict.fromkeys(ids))
print(len(ids))


df = pd.read_csv('EmbeddedData.csv')
df.info()
df = df.drop(columns=['Unnamed: 0'])
df = df[df['id'].isin(ids)]

df.to_csv("sampled_embeddings.csv")
df.info()